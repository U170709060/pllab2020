#include <stdio.h>
#include <stdlib.h>

int nrDigits(int num)
{
    static int count=0;

    if (num>0)
    {
        count++;
        nrDigits(num/10);
    }
    else
        return count;

}

int main(void)
{
    int count=0;
    int number;
    printf("Enter a positive integer number: ");
    scanf("%d", &number);
    int result = nrDigits(number);
    printf("Total digits in number %d", result);
    return(0);
}
